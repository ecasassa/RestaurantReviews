//
//  RestaurantReviewsApp.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

import SwiftUI

@main
struct RestaurantReviewsApp: App {
    @StateObject private var persistenceController = PersistenceController()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
