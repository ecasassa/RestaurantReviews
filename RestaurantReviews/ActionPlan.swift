//
//  ActionPlan.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

/*
  
 Thoughts as of this commit:
 - Got the stars working
 - Now that I think about it, I need to add an extra view between ContentView and ReviewRestaurantsView, so the user can see the reviews posted with the dates
    - I still need to make it so the user can add a review with a TextField and save that data, but I should make the view first with the plus button at the top.
 
 - I am coming up on the 4 hours. I started at 11am. It's almost 2pm. I have 1 hour left. I should be able to do it!
 
 
 
 
 If there's time:
 - Maybe add the number of reviews to ContentView instead of time for when it was edited
 - I highly doubt I'll be able to do a "Sort By" at the top with how little time I have
 
 */
 



 /*
 
 Plan:
 
 1. Build an app that works with CoreData
    - Add to the main tableview
    - Edit button for deleting
    - Detail page for the restaurants
 2. Add details to the Details page for reviews, and a slider for the stars, and probably a picker for the type of cuisine
 3. Make sure that it works and it makes sense
    - Do I want one page for adding and editing the restaurant, and another for the reviews? Or just put both in the same page? I feel like I won't have time for 2 separate ones
    - Start with both in the same Details page, and split them up later
 4. Check that it works on different screen sizes and with VoiceOver
 
 
 My initial thoughts on the action plan for this app
 
 - I have ~4 hours to work on it based on the instructions on the email
    - 2 hours of work, little break, and a couple more hours to try to do as much as possible
 

 Requirements:
 - Main page with a list of restaurants
    - Allow the user to add, edit, and delete restaurants to the main page's list
    - Display:
        - Restaurant name
        - type of cuisine
        - 1-5 stars given
 - Do a page where the user can edit the stars, and add reviews (notes) that are marked with a date
 
 - All on SwiftUI
 - Use CoreData for the data
 - No 3rd party libraries
 - Organized clean code
 
 */
