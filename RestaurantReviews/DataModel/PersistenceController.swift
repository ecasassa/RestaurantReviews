//
//  PersistenceController.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

import CoreData

class PersistenceController: ObservableObject {
    let container = NSPersistentContainer(name: "RestaurantModel")
    
    init() {
        container.loadPersistentStores { desc, error in
            if let error = error {
                print("Failed to load the data: \(error.localizedDescription)")
            }
        }
    }
    
    func save(context: NSManagedObjectContext) {
        do {
            try context.save()
            // send a message like a tick or something
            print("Data saved")
        } catch {
            // maybe add a message to the user that we couldn't save the data
            print("Could not save the data")
        }
    }
    
    func addRestaurant(name: String, cuisine: String, context: NSManagedObjectContext) {
        let restaurant = Restaurant(context: context)
        restaurant.id = UUID()
        restaurant.timestamp = Date()
        restaurant.name = name
        restaurant.cuisine = cuisine
        
        save(context: context)
    }
    
    func reviewRestaurant(restaurant: Restaurant, stars: Double, review: String, context: NSManagedObjectContext) {
        restaurant.timestamp = Date()
        restaurant.stars = stars
        restaurant.review = review
        
        save(context: context)
    }
}

