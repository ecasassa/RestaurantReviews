//
//  ContentView.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(sortDescriptors: [SortDescriptor(\.timestamp, order: .reverse)]) var restaurant: FetchedResults<Restaurant>
    
    @State private var showingAddView = false
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                List {
                    ForEach(restaurant) { restaurant in
                        NavigationLink(destination: ReviewRestaurantsView(restaurant: restaurant)) {
                            HStack {
                                ZStack {
                                    Image(systemName: "star.fill")
                                        .resizable()
                                        .frame(width: 50, height: 50)
                                    Text("\(restaurant.stars, specifier: "%.1f")")
                                        .foregroundColor(.black)
                                }
                                VStack(alignment: .leading, spacing: 6) {
                                    Text(restaurant.name ?? "")
                                        .bold()
                                    
                                    Text(restaurant.cuisine ?? "cuisine not selected").foregroundColor(.red)
                                }
                                Spacer()
                                Text(calcTimeSince(date: restaurant.timestamp ?? Date()))
                                    .foregroundColor(.gray)
                                    .italic()
                            }
                        }
                    }
                    .onDelete(perform: deleteRestaurant)
                }
                .listStyle(.plain)
            }
            .navigationTitle("Restaurant Reviews")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        showingAddView.toggle()
                    } label: {
                        Label("Add Restaurant", systemImage: "plus.circle")
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    EditButton()
                }
            }
            .sheet(isPresented: $showingAddView) {
                AddRestaurantsView()
            }
        }
        .navigationViewStyle(.stack)
    }
    
    private func deleteRestaurant(offsets: IndexSet) {
        withAnimation {
            offsets.map { restaurant[$0] }.forEach(managedObjectContext.delete)
                
            PersistenceController().save(context: managedObjectContext)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
