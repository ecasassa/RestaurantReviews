//
//  EditRestaurantsView.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

import SwiftUI

struct ReviewRestaurantsView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @Environment(\.dismiss) var dismiss
    
    var restaurant: FetchedResults<Restaurant>.Element
    
    @State private var name = ""
    @State private var cuisine = ""
    @State private var stars: Double = 0
    @State private var review = ""
    
    let restaurantTypes = ["American BBQ", "Latin", "Chinese", "Japanese", "Indian", "Italian", "French", "Greek", "Other"]
    
    var body: some View {
        HStack(alignment: .center) {
            Text("Review: " + (restaurant.name ?? "unknown restaurant"))
                .multilineTextAlignment(.center)
                .font(Font.system(size: 30, design: .default))
                .bold()
        }
        .padding()
        
        Form {
            Section {
                TextField("\(restaurant.name ?? "")", text: $name)
                    .multilineTextAlignment(.center)
                    .font(Font.system(size: 15, design: .default))
                    .onAppear {
                        name = restaurant.name ?? ""
                        cuisine = restaurant.cuisine ?? ""
                        stars = restaurant.stars
                        review = restaurant.review ?? ""
                    }
                VStack {
                    Text("Cuisine Type: " + (restaurant.cuisine ?? "unknown"))
                    Spacer()
                    Text("Star Rating: \(stars, specifier: "%.1f")")
                    Slider(value: $stars, in: 0...5, step: 0.1)
                }
                .padding()
                
                VStack {
                    Text(restaurant.review ?? "REVIEWS")
                }
            }
        }
        
        HStack {
            Spacer()
            Button("Submit") {
                PersistenceController().reviewRestaurant(restaurant: restaurant, stars: stars, review: review, context: managedObjectContext)
                dismiss()
            }
            Spacer()
        }
        .frame(maxHeight:.infinity, alignment: .bottom)
    }
}

struct EditRestaurantsView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
