//
//  AddRestaurantsView.swift
//  RestaurantReviews
//
//  Created by Etienne Casassa on 2/20/23.
//

import SwiftUI

struct AddRestaurantsView: View {
    @Environment(\.managedObjectContext) var managedObjectContext
    @Environment(\.dismiss) var dismiss
    
    @State private var name = ""
    @State private var cuisine = ""
    
    let restaurantTypes = ["American BBQ", "Latin", "Chinese", "Japanese", "Indian", "Italian", "French", "Greek", "Other"]
    
    var body: some View {
        Spacer()
        HStack(alignment: .center) {
            Spacer()
            Text("Add a Restaurant")
                .multilineTextAlignment(.center)
                .font(Font.system(size: 25, design: .default))
                .bold()
            Spacer()
        }
        .padding()
        Form {
            Section {
                TextField("Restaurant name", text: $name)
                
                VStack {
                    Picker("Cuisine Type:", selection: $cuisine) {
                        ForEach(restaurantTypes, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(.menu)
                }
                .padding()
                
                HStack {
                    Spacer()
                    Button("Submit") {
                        PersistenceController().addRestaurant(name: name, cuisine: cuisine, context: managedObjectContext)
                        dismiss()
                    }
                    Spacer()
                }
            }
        }
    }
}
struct AddRestaurantsView_Previews: PreviewProvider {
    static var previews: some View {
        AddRestaurantsView()
    }
}
